﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace unitypkgextractor
{
    class TarUtils
    {
        public static void ExtractTar(Stream stream, string outputDir) {
            var buffer = new byte[100];
            long lastRepPos = -100000000;
            while (true) {
                if(stream.Position - lastRepPos > (1024 * 1024)) {
                    Console.WriteLine("Picking apart tar... " + ByteLenghtToHumanReadable(stream.Position));
                    lastRepPos = stream.Position;
                }
                stream.Read(buffer, 0, 100);
                var name = Encoding.ASCII.GetString(buffer).Trim('\0').Trim('/');
                if (String.IsNullOrWhiteSpace(name))
                    break;
                stream.Seek(24, SeekOrigin.Current);
                stream.Read(buffer, 0, 12);
                var srrt = Encoding.ASCII.GetString(buffer, 0, 12);
                srrt = srrt.Replace(new string('\0',1), "");
                var size = Convert.ToInt64(srrt.Trim(), 8);

                stream.Seek(376L, SeekOrigin.Current);

                if(size == 0) {

                }
                else {
                    var output = Path.Combine(outputDir, name);
                    var dirpath = Path.GetDirectoryName(output);
                    if (!Directory.Exists(dirpath))
                        Directory.CreateDirectory(dirpath);
                    using (var str = File.Open(output, FileMode.OpenOrCreate, FileAccess.Write)) {
                        var buf = new byte[size];
                        stream.Read(buf, 0, buf.Length);
                        str.Write(buf, 0, buf.Length);
                    }
                }


                var pos = stream.Position;

                var offset = 512 - (pos % 512);
                if (offset == 512)
                    offset = 0;

                stream.Seek(offset, SeekOrigin.Current);
            }
        }

        public static void ExtractTarGz(string filename, string outputDir) {
            using (var stream = File.OpenRead(filename))
                ExtractTarGz(stream, outputDir);
        }

        public static void ExtractTarGz(Stream stream, string outputDir) {
            // A GZipStream is not seekable, so copy it first to a MemoryStream (max note: changed this to be filestream, as can be over 2gb+)
            using (var gzip = new GZipStream(stream, CompressionMode.Decompress)) {
                const int chunk = 1024 * 1024;
                Directory.CreateDirectory(outputDir);
                var tempFile = new FileInfo(Path.Combine(outputDir, "TEMPRAW" + new Random().Next()));
                using (var fileStrm = tempFile.Create()) {
                    int read;
                    var buffer = new byte[chunk];
                    long pos = 0;
                    do {
                        read = gzip.Read(buffer, 0, chunk);
                        fileStrm.Write(buffer, 0, read);
                        pos += read;
                        Console.WriteLine("Uncompressing gzip... " + ByteLenghtToHumanReadable(pos));
                    } while (read == chunk);

                    fileStrm.Seek(0, SeekOrigin.Begin);
                    ExtractTar(fileStrm, outputDir);                    
                }
                tempFile.Delete();
            }
        }

        public static void ExtractTar(string filename, string outputDir) {
            using (var stream = File.OpenRead(filename))
                ExtractTar(stream, outputDir);
        }

        public static string ByteLenghtToHumanReadable(long byteLenght, bool useBits = false) {
            string suffix = "";
            long lenght;
            if (useBits) lenght = byteLenght * 8;
            else lenght = byteLenght;

            if (lenght < 1024) {
                if (useBits) suffix = " bits";
                else suffix = " B";
                return lenght + suffix;
            }
            else if (lenght < 1048576) {
                if (useBits) suffix = " kbits";
                else suffix = " kB";
                return (lenght / 1024) + suffix;
            }
            else {
                if (useBits) suffix = " Mbits";
                else suffix = " MB";
                return System.Math.Round(((float)lenght / (float)1048576), 2) + suffix;
            }
        }
    }
}
