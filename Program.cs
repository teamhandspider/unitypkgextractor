﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace unitypkgextractor
{
    class Program
    {
        static void Main(string[] args) {

            string unityPkgPath;
            unityPkgPath = args.LastOrDefault();
            Console.WriteLine("Extracting unitypackage " + unityPkgPath);

            bool includeMetas = false;
            string finalFolderPath = "";

            var switches = args.Take(args.Length - 1).ToArray();
            
            for (int i = 0; i < switches.Length; i++) {
                var item = switches[i];
                if (item == "--metas" || item == "-m") {
                    includeMetas = true;
                }
                if (item == "outpath") {
                    finalFolderPath = switches[i+1];
                    i++;
                }
                else throw new System.Exception("unknown switch " + item);
            }

            if(includeMetas) {
                Console.WriteLine("\tIncluding meta files.");
            }
            else {
                Console.WriteLine("\tNot including meta files. Use the -m/--meta switch to include them.");
            }

            //unityPkgPath = @"C:\unpkgtest\[PS4 Plugins]Np Toolkit2_test2.unitypackage";
            //unityPkgPath = @"C:\Temp\TMP Examples & Extras.unitypackage";

            if (string.IsNullOrEmpty(unityPkgPath))
                throw new System.Exception("unityPkgPath null or empty!");

            var fileToDecompress = new FileInfo(unityPkgPath);

            var firstExtractPath = Path.Combine(fileToDecompress.Directory.FullName, fileToDecompress.Name.Substring(0, fileToDecompress.Name.Length - fileToDecompress.Extension.Length) + "_upkgRaw");
            Console.WriteLine();
            Console.WriteLine("Extracting gzip to get raw tar of the unitypackage...");
            Console.WriteLine("\t"+ firstExtractPath);
            if(Directory.Exists(firstExtractPath))
            {
                Console.WriteLine("\t_upkgRaw exists already, removing");
                Directory.Delete(firstExtractPath,true);
            }

            using (var ogFileStream = fileToDecompress.OpenRead()) {                
                TarUtils.ExtractTarGz(ogFileStream, firstExtractPath);
            }

            Console.WriteLine("Reorganizing extracted files to create the proper file structure...");
            if(finalFolderPath == "") {
                int numm = 0;
                do
                {
                    var postPart = "_upkgExtracted" + (numm == 0 ? "" : "(" + numm + ")");
                    finalFolderPath = Path.Combine(fileToDecompress.Directory.FullName, fileToDecompress.Name.Substring(0, fileToDecompress.Name.Length - fileToDecompress.Extension.Length) + postPart);
                    numm++;
                } while (Directory.Exists(finalFolderPath));
            }            
            

            var folFiles = new DirectoryInfo(firstExtractPath).GetDirectories();
            for (int i = 0; i < folFiles.Length; i++) {
                var folFile = folFiles[i];
                var folFileAssetPath = Path.Combine(folFile.FullName, "asset");

                var lines = File.ReadAllLines(Path.Combine(folFile.FullName, "pathname"));
                var fixdrelpath = lines[0];
                var extraPart = lines.Length == 2 ? lines[1] : "";
                var extraPartLogged = extraPart == "" ? "" : "extraPathPart:" + extraPart;
                bool isDirectory = !File.Exists(folFileAssetPath);
                Console.WriteLine("Moving asset "+folFile.Name+" to final path:"+fixdrelpath +" "+ extraPartLogged + (isDirectory ? "(IS FOLDER)" : "") );
                var fixdAbsfilPath = Path.Combine(finalFolderPath, fixdrelpath);

                if(isDirectory)
                {
                    //if (!Directory.Exists(folFileAssetPath)) throw new System.Exception(folFileAssetPath + ":Not a file or folder found wtf!");
                    Directory.CreateDirectory(folFileAssetPath);
                }
                else
                {
                    Directory.CreateDirectory(new FileInfo(fixdAbsfilPath).DirectoryName);
                    TryMoveFile(folFileAssetPath, fixdAbsfilPath);
                }
                             
                
                if(includeMetas) {
                    TryMoveFile(folFileAssetPath + ".meta", fixdAbsfilPath + ".meta");
                }
            }

            Directory.Delete(firstExtractPath, true);

            Console.WriteLine();
            Console.WriteLine("Done. Extracted "+ new FileInfo(unityPkgPath).Name+" to "+new DirectoryInfo(finalFolderPath).Name);
            Console.ReadKey();
        }

        private static void TryMoveFile(string folFileAssetPath, string fixdAbsfilPath) {
            try {
                File.Copy(folFileAssetPath, fixdAbsfilPath);
            }
            catch(System.Exception e) {
                Console.WriteLine("failure copying file! continuing anyway\n"+e);
                //System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
